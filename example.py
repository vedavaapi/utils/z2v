from z2v.lib.session import *
import json

from z2v.lib.zoom import ZoomApi
from pprint import pprint

session = AuthorizedSession()

session.set_access_token_from_file("/opt/z2v/zoom_access_token.json")

z = ZoomApi(session)

r = z.get_recordings('2020-01-05', '2020-04-05')
#  pprint(r.json())
with open('resp.json', 'wb') as f:
    f.write(json.dumps(r, indent=2, ensure_ascii=False).encode('utf-8'))
