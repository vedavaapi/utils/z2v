import os, webbrowser
import random, threading, logging

from wsgi_app import app

def initAuth():
    url = 'http://localhost:5000/oauth/init-zoom'.format()
    threading.Timer(1.25, lambda: webbrowser.open(url)).start()
    app.run(port=5000, debug=False)


if __name__ == '__main__':
    initAuth()
