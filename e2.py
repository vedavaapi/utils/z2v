from z2v.lib.session import *

from z2v.lib.vimeo import VimeoApi
import json

session = AuthorizedSession()

session.set_access_token_from_file("/opt/z2v/vimeo_access_token.json")

v = VimeoApi(session)

r = v.pull('https://us02web.zoom.us/rec/download/uMd5ceqp_2o3H9LAuQSDAfB7W43sev2sgXRL8_pbmk-9AiNRNAL1YOFDNuoWEcbDoqYGek5sgfntvg1o')
print(json.dumps(r.json(), indent=2, ensure_ascii=False))