import os

import flask_restx
import flask
from . import api


default_ns = api.namespace('default', path='/')

@default_ns.route('/<path:path>')
class StaticFile(flask_restx.Resource):
    def get(self, path):
        print(path)
        (current_dir, fname) = os.path.split(__file__)
        files_dir = os.path.join(current_dir, 'static')
        # return error_response(message='not implemented')
        return flask.send_from_directory(files_dir, path)


@default_ns.route('/')
class Default(flask_restx.Resource):
    def get(self):
        return flask.redirect('/index.html')
