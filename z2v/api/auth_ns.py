import json
import os
from time import time

import flask_restx
import flask
from requests import HTTPError
from furl import furl

from .. import zoom_client, zoom_scopes
from . import api


auth_ns = api.namespace('auth', path='/oauth')


def redirect_js(redirect_url):
    return f'redirecting you to app.... <script>window.location = "{redirect_url}";</script>'


def redirect_js_response(redirect_url, message_if_none='', message_if_invalid=''):
    if redirect_url is not None:
        redirect_furl = furl(redirect_url)
        if not redirect_furl.netloc:
            return {'message': message_if_invalid}, 200
        return flask.Response(redirect_js(redirect_furl.url))
    else:
        return {'message': message_if_none}, 200


@auth_ns.route('/init-zoom')
class ZoomOAuthInit(flask_restx.Resource):
    get_parser = auth_ns.parser()
    get_parser.add_argument('ui_redirect_uri', type=str, location='args', required=False)

    @auth_ns.expect(get_parser, validate=True)
    def get(self):
        args = self.get_parser.parse_args()
        zoom_callback_url = api.url_for(ZoomOAuthCallback, _external=True)
        redirect_url = zoom_client.auth_redirect_uri(
            zoom_callback_url, zoom_scopes, args.get('ui_redirect_uri', ''))
        return redirect_js_response(redirect_url, '', '')


def save_zoom_atr(atr):
    print(atr)
    atr['issued_at'] = time()
    flask.session['zoom_atr'] = atr


@auth_ns.route('/callback-zoom')
class ZoomOAuthCallback(flask_restx.Resource):
    get_parser = auth_ns.parser()
    get_parser.add_argument('code', type=str, location='args', required=True)
    get_parser.add_argument('state', type=str, location='args')

    @auth_ns.expect(get_parser, validate=True)
    def get(self):
        args = self.get_parser.parse_args()
        code = args['code']
        zoom_callback_url = api.url_for(ZoomOAuthCallback, _external=True)
        ui_redirect_url = args.get('state', '')

        try:
            atr = zoom_client.exchange_code_for_access_token(code, zoom_callback_url)
        except HTTPError:
            return {"error": "error in auth flow"}, 400

        # TODO check restricted user thing.
        save_zoom_atr(atr)

        if ui_redirect_url:
            print(ui_redirect_url)
            return redirect_js_response(ui_redirect_url)
        return {"status": "auth setup successfull"}, 200


@auth_ns.route('/logout')
class Logout(flask_restx.Resource):

    def get(self):
        flask.session.pop('zoom_atr', None)
        return {"status": "logout success"}, 200


@auth_ns.route('/me')
class Me(flask_restx.Resource):

    def get(self):
        if not flask.session.get('zoom_atr', None):
            return {"error": "not authorized"}, 401
        return flask.session.get('zoom_atr')
