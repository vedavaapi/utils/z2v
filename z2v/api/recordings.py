import os
from time import time

import flask_restx
import flask
from requests import HTTPError
from furl import furl

from .. import zoom_client, zoom_scopes
from ..lib.session import AuthorizedSession
from ..lib.zoom import ZoomApi
from . import api


recordings_ns = api.namespace('recordings', path='/recordings')


def error_response(code, message):
    resp = flask.make_response(
        flask.jsonify({
            "error": message,
            'code': code
        }),
        code
    )
    return resp


def get_effective_atr(atr):
    effective_atr = atr
    if zoom_client.is_expired(atr):
        effective_atr = zoom_client.refresh_access_token(atr['refresh_token'])
        flask.session['atr'] = effective_atr
    return effective_atr


@recordings_ns.route('')
class Recordings(flask_restx.Resource):
    get_parser = api.parser()
    get_parser.add_argument('from', location='args', type=str, required=True)
    get_parser.add_argument('to', location='args', type=str, required=False)

    @recordings_ns.expect(get_parser, validate=True)
    def get(self):
        args = self.get_parser.parse_args()
        atr = flask.session.get('zoom_atr', None)

        if not atr:
            return error_response(401, 'not authorized')

        try:
            effective_atr = get_effective_atr(atr)
        except HTTPError as e:
            flask.session.pop('zoom_atr', None)
            return error_response(401, 'not authorized')
        # print(effective_atr)
        zoom_session = AuthorizedSession(access_token=effective_atr['access_token'])
        zoom_api = ZoomApi(zoom_session)

        meetings = zoom_api.get_recordings(args['from'], args.get('to', None))

        return meetings
