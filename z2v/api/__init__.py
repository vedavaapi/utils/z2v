import flask_restx
from flask import Blueprint


api_blueprint_v1 = Blueprint('z2v' + '_v1', __name__)

api = flask_restx.Api(
    app=api_blueprint_v1,
    version='1.0',
    title='z2v',
    doc='/docs'
)


def push_environ_to_g():
    pass


api_blueprint_v1.before_request(push_environ_to_g)


from .auth_ns import auth_ns
from .recordings import recordings_ns
from .rest import default_ns
