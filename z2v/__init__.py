import json
import flask
from flask_cors import CORS

from .lib.oauth import ZoomClient


app = flask.Flask(__name__, instance_relative_config=True)
CORS(app)
@app.after_request
def allow_creds(response):
    response.headers["Access-Control-Allow-Credentials"] = "true"
    return response


try:
    app.config.from_json(filename="config.json")
except FileNotFoundError as e:
    pass


zoom_client_creds = json.loads(open(app.config.get('ZOOM_CLIENT_CREDS_PATH'), 'rb').read())
zoom_client = ZoomClient(zoom_client_creds)
zoom_scopes = app.config.get('ZOOM_SCOPES', ['public', 'private', 'create', 'edit', 'upload'])


from z2v.api import api_blueprint_v1
app.register_blueprint(api_blueprint_v1, url_prefix='')
