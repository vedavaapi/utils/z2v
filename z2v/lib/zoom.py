from z2v.lib.session import AuthorizedSession
from datetime import date, timedelta

class ZoomApi(object):

    api_base_url = 'https://api.zoom.us/v2'

    def __init__(self, session: AuthorizedSession):
        self.session = session

    def _get_recordings(self, from_date=None, to_date=None, next_page_token=None):
        recordings_api_url = f'{self.api_base_url}/users/me/recordings'

        params = {}
        if from_date:
            params['from'] = from_date
        if to_date:
            params['to'] = to_date
        if next_page_token:
            params['next_page_token'] = next_page_token

        # print('querying', params)
        resp = self.session.get(recordings_api_url, params=params)
        #  print(resp.json())
        return resp

    def collect_recordings(self, f, t):
        meetings = []

        exhausted = False
        next_page_token = None
        while not exhausted:
            resp = self._get_recordings(from_date=f, to_date=t, next_page_token=next_page_token)
            if (resp.status_code != 200):
                print(resp.json())
            resp.raise_for_status()
            resp_data = resp.json()
            # print(resp_data, exhausted)
            next_page_token = resp_data['next_page_token']
            exhausted = not next_page_token
            meetings.extend(resp_data['meetings'])

        return meetings

    def get_recordings(self, from_date, to_date=None):
        if not to_date:
            to_date = str(date.today())
        fd = date(*(map(int, from_date.split('-'))))
        td = date(*(map(int, to_date.split('-'))))

        month_delta = timedelta(days=29)
        next_to = fd + month_delta

        print("fd", fd, 'td', td)

        meetings = []
        while True:
            print(next_to)
            next_to = next_to + month_delta
            next_meetings = self.collect_recordings(str(fd), str(td))
            meetings.extend(next_meetings)
            if next_to > td:
                break

        return meetings
