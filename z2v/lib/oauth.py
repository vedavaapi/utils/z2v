import logging
from time import time

import requests
from furl import furl


logging.basicConfig(
    level=logging.DEBUG,
    format="%(levelname)s: %(asctime)s {%(filename)s:%(lineno)d}: %(message)s "
)


class OauthClientsRegistry(object):
    client_classes = {}

    @classmethod
    def register_client_class(cls, client_class):
        cls.client_classes[client_class.provider_name] = client_class

    @classmethod
    def get_client_class(cls, provider_name):
        return cls.client_classes.get(provider_name, None)


class OAuthClient(object):

    provider_name = 'provider'
    auth_uri = ''
    token_uri = ''

    # noinspection PyUnusedLocal
    def __init__(self, *args, **kwargs):
        pass

    # step1
    def auth_redirect_uri(self, callback_url, scope, state=''):
        pass

    # step2
    def extract_auth_code(self, args):
        pass

    # step3
    def exchange_code_for_access_token(self, auth_code, registered_callback_url):
        pass

    def extract_access_token_from_response(self, atr):
        pass


class ZoomClient(OAuthClient):

    provider_name = 'zoom'
    auth_uri = 'https://zoom.us/oauth/authorize'
    token_uri = 'https://zoom.us/oauth/token'

    def __init__(self, client_creds):
        super(ZoomClient, self).__init__()
        self.client_creds = client_creds

    def auth_redirect_uri(self, callback_url, scope, state=''):
        params = {
            'response_type': 'code',
            'client_id': self.client_creds['client_id'],
            'redirect_uri': callback_url,
            'scope': scope,
            'state': state
        }

        auth_furl = furl(self.auth_uri)
        auth_furl.args.update(params)
        return auth_furl.url

    def extract_auth_code(self, args):
        return args['code']

    def _get_auth_basic_header(self):
        from base64 import b64encode
        return 'Basic ' + b64encode((self.client_creds['client_id'] + ':' + self.client_creds['client_secret']).encode('utf-8')).decode('utf-8')

    def exchange_code_for_access_token(self, auth_code, registered_callback_url):

        request_data = {
            'grant_type': 'authorization_code',
            'redirect_uri': registered_callback_url,
            'code': auth_code
        }

        atr = requests.post(self.token_uri, data=request_data, headers={
            'Authorization': self._get_auth_basic_header()
        })
        atr.raise_for_status()
        return atr.json()

    def extract_access_token_from_response(self, atr):
        if not atr:
            return None
        return atr.get('access_token', None)

    def is_expired(self, atr):
        return atr.get('issued_at', 0) + (atr.get('expires_in') - 300) <= time()

    def refresh_access_token(self, refresh_token):
        request_data = {
            'grant_type': 'refresh_token',
            'refresh_token': refresh_token,
        }

        atr = requests.post(self.token_uri, data=request_data, headers={
            'Authorization': self._get_auth_basic_header()
        })
        #  print(atr.json())
        atr.raise_for_status()
        return atr.json()


OauthClientsRegistry.register_client_class(ZoomClient)
