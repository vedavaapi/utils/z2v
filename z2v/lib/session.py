import json

import requests


class AuthorizedSession(object):
    def __init__(self, access_token=None):
        self.session = requests.Session()
        self.access_token = access_token

    def set_access_token(self, access_token):
        self.access_token = access_token

    @classmethod
    def authorization_header(cls, access_token):
        return 'Bearer {}'.format(access_token) if access_token else None

    @classmethod
    def authorized_headers(cls, headers, access_token):
        if not access_token:
            return
        new_headers = headers.copy()
        new_headers['Authorization'] = cls.authorization_header(access_token)
        return new_headers

    def set_access_token_from_file(self, file_path):
        creds = json.loads(open(file_path, 'rb').read().decode('utf-8'))
        access_token = creds['access_token']
        self.set_access_token(access_token)

    def get(self, url, params=None, authorize_request=True, **kwargs):
        headers = kwargs.pop('headers', {})
        if authorize_request:
            headers = self.authorized_headers(headers, self.access_token)

        return self.session.get(url, params=params, headers=headers, **kwargs)

    def post(self, url, data=None, files=None, authorize_request=True, **kwargs):
        headers = kwargs.get('headers', {})
        if authorize_request:
            headers = self.authorized_headers(headers, self.access_token)

        return self.session.post(url, data=data, files=files, headers=headers, **kwargs)

    def put(self, url, data=None, files=None, authorize_request=True, **kwargs):
        headers = kwargs.get('headers', {})
        if authorize_request:
            headers = self.authorized_headers(headers, self.access_token)

        return self.session.put(url, data=data, files=files, headers=headers, **kwargs)

    def delete(self, url, data=None, authorize_request=True, **kwargs):
        headers = kwargs.get('headers', {})
        if authorize_request:
            headers = self.authorized_headers(headers, self.access_token)

        return self.session.delete(url, data=data, headers=headers, **kwargs)
